FROM mcr.microsoft.com/dotnet/sdk:5.0


ENV PATH="/root/.dotnet/tools:${PATH}"

RUN dotnet tool install --global GitVersion.Tool
